using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Rigidbody))]
public class PlayerIso : MonoBehaviour {
	
	public float movementSpeed = 2f, selfStunTime = 2f;
	public Skill[] skills;
	public LayerMask clickMovementLayer;
	public GameObject waypointIndicator;
	public bool invisible{get;set;}
	public bool pouncing{get;set;}

	protected Rigidbody rigidb{get;set;}

	private Vector3 moveTarget;
	private bool moving, stunned;
	private GameObject oldWaypoint;
	private NavMeshAgent navMesh;
	private Color[] defaultColors;


	protected virtual void Awake () {
		this.gameObject.tag = "Player";
		invisible = false;
		rigidb = GetComponent<Rigidbody>();
		skills = GetComponentsInChildren<Skill>();
		navMesh = GetComponent<NavMeshAgent>();
		defaultColors = new Color[GetComponentsInChildren<Renderer> ().Length];
		for (int i = 0; i < GetComponentsInChildren<Renderer> ().Length; i++) {
			Renderer renderer = GetComponentsInChildren<Renderer> () [i];
			defaultColors[i] = renderer.material.color;
		}
	}

	protected virtual void FixedUpdate () {
		if(stunned){
			return;
		}
		PlayerMovement();
		ProcessSkillInputs();
	}
	
	protected void PlayerMovement (){
		if(Input.GetMouseButton(1)){
			Ray clickRay = Camera.main.ScreenPointToRay(Input.mousePosition);
			
			RaycastHit rayHitInfo;
			//TODO: add LayerMask.
			if(Physics.Raycast(clickRay, out rayHitInfo, Mathf.Infinity, clickMovementLayer )){
				moveTarget = rayHitInfo.point;
				CreateNewWaypoint(moveTarget);
				navMesh.SetDestination (moveTarget);
			}
		}
		if(Vector3.Distance(navMesh.destination, this.transform.position) < 0.02f){
			if(oldWaypoint != null){
				Destroy(oldWaypoint.gameObject);
			}
		}
	}

	protected void CreateNewWaypoint(Vector3 spawnTarget){
		if(oldWaypoint != null){
			Destroy(oldWaypoint);
		}
		GameObject newWaypoint = Instantiate (waypointIndicator, (spawnTarget /*+ new Vector3(0f, 2.5f, 0f)*/), Quaternion.Euler(new Vector3(90f,0f,0f))) as GameObject;
		oldWaypoint = newWaypoint;
	}

	protected virtual void ProcessSkillInputs(){
		foreach(var skill in skills){
			if(skill.ActivationInputDetected()){
				EndPendingSkills();
				skill.Activate();
				return;
			}
		}
	}

	private void EndPendingSkills(){
		foreach(Skill skill in skills){
			skill.EndAbility();
		}
	}

	public void OnCollisionEnter(Collision collisionInfo){
		if(pouncing){
			if(collisionInfo.gameObject.layer == LayerMask.NameToLayer("Floor") == false){
				//TODO: Player gets stunned for a while
				Debug.Log(collisionInfo.collider.name);
				Stun (selfStunTime);
				GetComponentInParent<Animator>().SetTrigger("Stun");
			}
		}
	}


	private void Stun(float stunTime){
		stunned = true;
		/*
		for (int i = 0; i < GetComponentsInChildren<Renderer> ().Length; i++) {
			Renderer renderer = GetComponentsInChildren<Renderer> () [i];
			renderer.material.color = Color.cyan; 
		}
		*/
		//GetComponent<Renderer>().material.color = Color.cyan;
		pouncing = false;

		navMesh.SetDestination(this.transform.position);
		Invoke ("UnStun", selfStunTime);
		//TODO little animation?
	}

	public void UnStun(){
		stunned = false;
		for (int i = 0; i < GetComponentsInChildren<Renderer> ().Length; i++) {
			Renderer renderer = GetComponentsInChildren<Renderer> () [i];
			renderer.material.color = defaultColors[i]; 
		}

		//TODO little animation?
	}

}
