﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Rigidbody))]
public class PatrolWaypointsAnimated : MonoBehaviour {
	
	public Transform[] patrolWaypoints;
	public float moveSpeed = 0.5f;
	public float restartPatrolDelay = 5f;
	
	public bool drawGizmo;

	//TODO StateMachine
	private Animator anim;
	private Rigidbody rigidb;
	
	void Start () {
		rigidb = GetComponent<Rigidbody>();
		anim = this.GetComponent<Animator>();
		this.transform.position = patrolWaypoints[0].position;
		StartCoroutine( StartPatrol_Coro() );
	}
	
	protected IEnumerator StartPatrol_Coro(){
		Transform[] reversePatrolWaypoints = patrolWaypoints.Clone() as Transform[];
		Array.Reverse(reversePatrolWaypoints);
		
		while(true){
			foreach(Transform transf in patrolWaypoints){
				yield return StartCoroutine ( MoveToWaypoint(transf.position) );
			}
			yield return new WaitForSeconds(restartPatrolDelay);
			foreach(Transform transf in reversePatrolWaypoints){
				yield return StartCoroutine ( MoveToWaypoint(transf.position) );
			}
			yield return new WaitForSeconds(restartPatrolDelay);
		}
	}
	
	private IEnumerator MoveToWaypoint(Vector3 targetWaypoint){
		this.transform.LookAt(targetWaypoint);
		anim.SetFloat("vertical", 1f);
		while( Vector3.Distance(this.rigidb.position, targetWaypoint) > 0.1f ){
			Vector3 newPosition = Vector3.MoveTowards(this.transform.position, targetWaypoint, moveSpeed * Time.deltaTime);
			//this.rigidb.MovePosition( newPosition );
			yield return null;
		}
		anim.SetFloat("vertical", 0f);
	}
	
	void OnDrawGizmos(){
		if(drawGizmo){
			if(patrolWaypoints==null || patrolWaypoints.Length ==0){
				return;
			}
			Transform previous = patrolWaypoints[0];
			for(int i=1; i<patrolWaypoints.Length; i++){
				Gizmos.color = Color.red;
				Gizmos.DrawLine(previous.position, patrolWaypoints[i].position);
				previous = patrolWaypoints[i];
			}
		}
	}
}
