﻿using UnityEngine;
using System.Collections;

public class FreezeRotation : MonoBehaviour {

	public bool freezeX, freezeY, freezeZ;

	private Vector3 rotationToKeep;

	void Start(){
		rotationToKeep = this.transform.eulerAngles;
	}

	void LateUpdate () {
		Vector3 newRotation = this.transform.eulerAngles;
		if(freezeX){
			newRotation.x = rotationToKeep.x;
		}
		if(freezeY){
			newRotation.y = rotationToKeep.y;
		}
		if(freezeZ){
			newRotation.z = rotationToKeep.z;
		}
		this.transform.eulerAngles = newRotation;
	}

}
