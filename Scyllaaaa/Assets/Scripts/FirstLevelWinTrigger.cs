﻿using UnityEngine;
using System.Collections;

public class FirstLevelWinTrigger : MonoBehaviour {

	public void OnTriggerEnter(Collider other){
		if(other.gameObject.CompareTag("Player")){
			Application.LoadLevel(2);
		}
	}
}
