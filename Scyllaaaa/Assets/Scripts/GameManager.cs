﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public GameObject[] levelPrefabs;
	public float enemySpotDistance = 20f;
	public Text objectiveText, timerText, winText;
	public float timeToFinish;
	public bool timerMode;
	public GameObject loosePanel;
	public GameObject currentLevel = null;

	public Enemy[] enemies{get;set;}
	private int level = 0;
	private GameObject player;
	private GameObject restartLevel = null;
	private int enemiesLeft;
	private bool levelLoaded;

	private float timeOfLevelStart;


	public void Awake(){
		enemiesLeft = GameObject.FindGameObjectsWithTag("Enemy").Length;
		winText.text = "";
		objectiveText.text = "OBJECTIVE: Escape";
		if(timerMode){
			timerText.text = ""+timeToFinish;
		}
	}

	public void Start(){	
		/*
		if(loadLevel){
			LoadNextLevel();
		} */
		StartLevel();
		FindAndStoreEnemiesAndPlayer ();
	}

	public void StartLevel(){
		if(restartLevel == null){
			restartLevel = Instantiate(currentLevel) as GameObject;
			restartLevel.SetActive(false);
			currentLevel = restartLevel;
			timeOfLevelStart = Time.time;
		} else {
			Destroy (currentLevel);
			restartLevel.SetActive(true);
			timeOfLevelStart = Time.time;
			timerText.text = ""+timeToFinish;
		}
		FindAndStoreEnemiesAndPlayer ();
	}

	public void Update(){
		if(timerMode){
			float timeLeft = timeToFinish - (Time.time -timeOfLevelStart);
			timeLeft = (float) Math.Round(timeLeft, 2);
			timerText.text = ""+timeLeft;
			if(timeLeft <= 0 ){
				GameOver();
			}
		}
		SimulateFogOfWarForEnemies();

	}

	private void FindAndStoreEnemiesAndPlayer ()
	{
		GameObject[] enemiesGameObjs = GameObject.FindGameObjectsWithTag ("Enemy");
		enemies = new Enemy[enemiesGameObjs.Length];
		for (int i = 0; i < enemiesGameObjs.Length; i++) {
			enemies [i] = enemiesGameObjs [i].GetComponent<Enemy> ();
		}
		player = GameObject.FindGameObjectWithTag("Player");
	}

	public void GameOver(){
		winText.text = "Game Over!";
		loosePanel.SetActive(true);
		Time.timeScale = 0f;
	}
	/*
	public void LoadLevel(int level){
		if(currentLevelInstance != null){
			Destroy(currentLevelInstance);
		}

		if(level > levelPrefabs.Length){
			Debug.LogError("ERROR");
		} else {
			currentLevelInstance = Instantiate(levelPrefabs[level-1], Vector3.zero, Quaternion.identity) as GameObject;
			FindAndStoreEnemiesAndPlayer();
			levelLoaded = true;
		}
	}

	public void LoadNextLevel(){
		if(currentLevelInstance != null){
			Destroy(currentLevelInstance);
		}

		level++;
		if(level > levelPrefabs.Length){
			VictoryScreen();
		} else {
			currentLevelInstance = Instantiate(levelPrefabs[level-1], Vector3.zero, Quaternion.identity) as GameObject;
			FindAndStoreEnemiesAndPlayer();
		}

	}
*/
	public void VictoryScreen(){
		winText.text = "HOLY SHIT YOU BEAT THE GAME, THAT WAS SOME MONSTER PERFORMANCE BRAH!! \nI'M PROUD OF YOU!";
		Application.LoadLevel(3);
		Time.timeScale = 0f;
	}

	public void DecreaseEnemiesLeft(){
		enemiesLeft--;
		if(enemiesLeft == 0){
			objectiveText.text = "Exit the level to WIN!!";
		}
		GetComponentInChildren<BoxCollider>().enabled = true;
	}

	public void RestartLevel(){
		Application.LoadLevel(Application.loadedLevel);
	}

	public void OnTriggerEnter(Collider other){
		VictoryScreen();
	}

	public void SimulateFogOfWarForEnemies(){
		foreach(Enemy enemy in enemies){
			NavMeshPath path = new NavMeshPath();
			player.GetComponent<NavMeshAgent>().CalculatePath(enemy.transform.position, path);
			if(PathLength(path) < enemySpotDistance){
				foreach(Renderer renderer in enemy.GetComponentsInChildren<Renderer>()){
					renderer.enabled = true;
				}
			} else {
				foreach(Renderer renderer in enemy.GetComponentsInChildren<Renderer>()){
					renderer.enabled = false;
				}
			}
		}
	}

	private float PathLength(NavMeshPath path) {
		if (path.corners.Length < 2)
			return 0;
		
		Vector3 previousCorner = path.corners[0];
		float lengthSoFar = 0.0F;
		int i = 1;
		while (i < path.corners.Length) {
			Vector3 currentCorner = path.corners[i];
			lengthSoFar += Vector3.Distance(previousCorner, currentCorner);
			previousCorner = currentCorner;
			i++;
		}
		return lengthSoFar;
	}

	public void CloseGame(){
		Application.Quit();
	}
}
