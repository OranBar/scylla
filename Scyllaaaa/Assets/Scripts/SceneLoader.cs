﻿using UnityEngine;
using System.Collections;

public class SceneLoader : MonoBehaviour {

	public void LoadScene(int i){
		Time.timeScale = 1f;
		Application.LoadLevel(i);
	}
}
