﻿using UnityEngine;
using System.Collections;
using OranUnityUtils;

public class Enemy : MonoBehaviour {

	public float coneAngle = 30f;
	public float coneRange = 6f;
	public bool drawGizmo;
	public int chaseForXFrames = 180;

	private PlayerIso player;
	private Color[] defaultColors;
	private GameManager gameManager;
	private PatrolWaypoints patrolScript;
	private bool stunned = false, pausingUpdates = false;

	private bool rescuingStunnedEnemy, chasingPlayer;
	private Transform enemyToRescue;
	private int framesWihoutLineOfSightToPlayer;

	void Awake () {
		this.gameObject.tag = "Enemy";
		gameManager = GameObject.FindObjectOfType<GameManager>();
		player = GameObject.FindObjectOfType<PlayerIso>();
		defaultColors = new Color[GetComponentsInChildren<Renderer> ().Length];
		for (int i = 0; i < GetComponentsInChildren<Renderer> ().Length; i++) {
			Renderer renderer = GetComponentsInChildren<Renderer> () [i];
			defaultColors[i] = renderer.material.color;
		}
		patrolScript = GetComponent<PatrolWaypoints>();
	}
	
	void Update () {
		if(stunned || pausingUpdates){
			return;
		}

		if( LookForPlayerAndChaseIfFound () ){
			return;
		} 

		if(!rescuingStunnedEnemy){
			LookForEnemyToRescue ();
		} 
		if(rescuingStunnedEnemy) {
			if( Vector3.Distance (this.transform.position, enemyToRescue.position) < 1.7f ){
				RescueEnemy ();
			}
		}

		if(Vector3.Distance(this.transform.position, player.transform.position) < 2.5f){
			GameObject.FindObjectOfType<GameManager>().GameOver();
		}

	}

	private bool LookForPlayerAndChaseIfFound () {
		bool result = false;
		if (CheckIfInSight (player.transform)) {
			chasingPlayer = true;
			patrolScript.Stop ();
			this.GetComponent<NavMeshAgent> ().SetDestination(player.transform.position);
			framesWihoutLineOfSightToPlayer = chaseForXFrames;
			for (int i = 0; i < GetComponentsInChildren<Renderer> ().Length; i++) {
				Renderer renderer = GetComponentsInChildren<Renderer> () [i];
				renderer.material.color = Color.red;
			}

			//		Debug.Log ("MONSTER IN SIGHT");
			result = true;
		}
		if (chasingPlayer) {
			//Enemy was seen but now isn't in sight
			framesWihoutLineOfSightToPlayer--;
			result = true;
		}
		if (framesWihoutLineOfSightToPlayer <= 0) {
			chasingPlayer = false;
			patrolScript.Resume ();

			for (int i = 0; i < GetComponentsInChildren<Renderer> ().Length; i++) {
				Renderer renderer = GetComponentsInChildren<Renderer> () [i];
				renderer.material.color = defaultColors[i]; 
			}
		}
		return result;
	}

	void LookForEnemyToRescue ()
	{
		foreach (Enemy enemy in GameObject.FindObjectOfType<GameManager> ().enemies) {
			if (enemy.stunned && CheckIfInSight (enemy.transform)) {
				patrolScript.Stop ();
				GetComponent<NavMeshAgent> ().SetDestination (enemy.transform.position);
				enemyToRescue = enemy.transform;
				rescuingStunnedEnemy = true;
				break;
			}
		}
	}

	void RescueEnemy ()
	{
		rescuingStunnedEnemy = false;
		pausingUpdates = true;
		this.StartCoroutineTimeline (WaitTime (1f), this.ToIEnum (() => UnstunEnemy ()));
	}

	public void UnstunEnemy(){
		rescuingStunnedEnemy = false;
		enemyToRescue.GetComponent<Enemy>().UnStun();
		enemyToRescue = null;
		patrolScript.Resume();
		pausingUpdates = false;
	}
	
	public IEnumerator WaitTime(float timeToWait){
		yield return new WaitForSeconds(timeToWait);
	}

	public bool CheckIfInSight(Transform targetTransf){
		PlayerIso targetPlayer = targetTransf.GetComponent<PlayerIso>();
		if(targetPlayer != null && targetPlayer.invisible){
			return false;
		}
		Vector3 playerDirection = targetTransf.position - this.transform.position;
		if( Vector3.Angle(this.transform.forward, playerDirection) <= coneAngle ){
			if( Vector3.Distance(this.transform.position, targetTransf.position) < coneRange){
				RaycastHit rayHit;
				Ray sightRay = new Ray(this.transform.position, transform.forward);
				if( Physics.Raycast(sightRay, out rayHit) ){
					if(rayHit.collider.transform == targetTransf){
						return true;
					}
				}
			}
		}
		return false;
	}

	void OnDrawGizmos(){
		if(drawGizmo){
			Vector3 endLine = this.transform.position + (this.transform.forward * coneRange);
			Gizmos.color = Color.yellow;
			Gizmos.DrawLine(this.transform.position, endLine);
		}
	}

	public void OnDestroy(){
		try{
			gameManager.DecreaseEnemiesLeft();
		}catch(MissingReferenceException e){

		}
	}

	public void Stun(float stunDuration){
		patrolScript.Stop();
		stunned = true;
		//TODO
		for (int i = 0; i < GetComponentsInChildren<Renderer> ().Length; i++) {
			Renderer renderer = GetComponentsInChildren<Renderer> () [i];
			renderer.material.color = Color.cyan;
		}
		Invoke ("UnStun", stunDuration);
	}

	private void UnStun(){
		stunned = false;
		for (int i = 0; i < GetComponentsInChildren<Renderer> ().Length; i++) {
			Renderer renderer = GetComponentsInChildren<Renderer> () [i];
			renderer.material.color = defaultColors[i]; 
		}
		patrolScript.Resume();
	}

	public void Stop(){
		patrolScript.Stop();
	}
	/*
	public void OnTriggerEnter(Collider other){
		if(other.gameObject.CompareTag ("Player")){
			GameObject.FindObjectOfType<GameManager>().GameOver();
		}
	}
	*/
}
