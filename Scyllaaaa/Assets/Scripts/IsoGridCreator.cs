using UnityEngine;
using System.Collections;

public class IsoGridCreator : MonoBehaviour {
	
	public GameObject tile;
	public float tileSideLength = 1f;
	public int rows=8, columns=8;
	public GameObject[,] matrix {get;set;}
	
	
	private Transform gridHolder;
	
	
	private void Start () {
		gridHolder = new GameObject("Board Holder").transform;
		CreateGrid ();
	//	CenterCamera();
	}
	
	private void CreateGrid () {
		matrix = new GameObject[columns, rows];
		for (int x = 0; x < columns; x++) {
			for (int z = 0;  z< rows; z++) {
				Vector3 position = new Vector3 (x * tileSideLength, 0f, z * tileSideLength);
				GameObject tileInstance = Instantiate (tile, position, Quaternion.identity) as GameObject;
				matrix [x, z] = tileInstance;
				tileInstance.transform.SetParent (gridHolder);
			}
		}
	}
	
	private void CenterCamera(){
		Vector3 position = Camera.main.transform.position;
		int temp = Mathf.Max(columns, rows);
		position.x = temp/2;
		position.y = temp/2;
		Camera.main.transform.position = position;
	}
	
}
