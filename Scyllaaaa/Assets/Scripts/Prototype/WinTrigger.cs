﻿using UnityEngine;
using System.Collections;

public class WinTrigger : MonoBehaviour {

	public void OnTriggerEnter(Collider other){
		if(other.gameObject.CompareTag("Player")){
			GameObject.FindObjectOfType<GameManager>().VictoryScreen();
		}
	}
}
