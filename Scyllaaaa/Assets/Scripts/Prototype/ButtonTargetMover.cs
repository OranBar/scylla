﻿using UnityEngine;
using System.Collections;

public class ButtonTargetMover : Interactable {

	public GameObject target;
	public Transform moveTarget;
	public float moveSpeed;

	public override void Interact (){
		StartCoroutine( MoveToPoint(moveTarget.position) );
	}

	private IEnumerator MoveToPoint(Vector3 targetWaypoint){
		this.transform.LookAt(targetWaypoint);
		while( Vector3.Distance(target.transform.position, targetWaypoint) > 0.1f ){
			Vector3 newPosition = Vector3.MoveTowards(target.transform.position, targetWaypoint, moveSpeed * Time.deltaTime);
			target.GetComponent<Rigidbody>().MovePosition( newPosition );
			yield return null;
		}
	}
}
