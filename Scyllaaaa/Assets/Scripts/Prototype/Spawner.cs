﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

	public GameObject enemy;
	public Transform topSpawn;
	public Transform bottomSpawn;
	public Transform leftSpawn;
	public Transform rightSpawn;
	public float spawnLength;
	public float spawnDelay;
	public float enemySpeed;

	private Transform[] spawnPositions;


	void Start () {
		spawnPositions = new Transform[4];
		spawnPositions[0] = topSpawn;
		spawnPositions[1] = bottomSpawn;
		spawnPositions[2] = leftSpawn;
		spawnPositions[3] = rightSpawn;
		StartCoroutine( StartSpawning(spawnDelay) );
	}

	private IEnumerator StartSpawning(float delay){
		while(true){
			SpawnEnemy();
			yield return new WaitForSeconds(spawnDelay);
		}
	}

	private void SpawnEnemy(){
		int randomIndex = Random.Range( 0, spawnPositions.Length );
		Vector3 position = spawnPositions[randomIndex].position;
		GameObject enemyInstance = null;

		if(randomIndex <= 1 ){
			float positionOffset = Random.Range(-spawnLength, spawnLength);
			position.x += positionOffset; 
			enemyInstance = Instantiate(enemy, position, Quaternion.identity ) as GameObject;
		}
		else if(randomIndex >= 2 ){
			float positionOffset = Random.Range(-spawnLength, spawnLength);
			position.y += positionOffset; 
			enemyInstance = Instantiate(enemy, position, Quaternion.identity ) as GameObject;
		}

		if(spawnPositions[randomIndex] == topSpawn){
			enemyInstance.GetComponent<Rigidbody2D>().AddForce( Vector3.down * enemySpeed );
		}
		if(spawnPositions[randomIndex] == bottomSpawn){
			enemyInstance.GetComponent<Rigidbody2D>().AddForce( Vector3.up * enemySpeed );
		}
		if(spawnPositions[randomIndex] == leftSpawn){
			enemyInstance.GetComponent<Rigidbody2D>().AddForce( Vector3.right * enemySpeed );
		}
		if(spawnPositions[randomIndex] == rightSpawn){
			enemyInstance.GetComponent<Rigidbody2D>().AddForce( Vector3.left * enemySpeed);
		}
	}
}
