﻿using UnityEngine;
using System.Collections;

public class StingAttack : ActivationSkills {

	public float stunDuration = 10f;
	public float range;

	protected override float GetRange ()
	{
		return range;
	}

	public override bool ActivationInputDetected (){
		return Input.GetKeyDown (KeyCode.Q);
	}

	protected override void UseSkillImpl (RaycastHit mouseHit){
		bool hitEnemy = mouseHit.collider.CompareTag ("Enemy");
		bool enemyInRange = Vector3.Distance(this.transform.position, mouseHit.transform.position) < range; 
		if(hitEnemy && enemyInRange && CheckIfPlayerBehindTransf(mouseHit.transform)){
			RaycastHit rayHit;

			Physics.Linecast(this.transform.position, mouseHit.transform.position, out rayHit);
			if(rayHit.transform != null && rayHit.transform.CompareTag("Enemy")){
				this.timeWhenActivated = Time.time;

				AudioSource audioSource = GetComponentInParent<AudioSource>();
				audioSource.clip = skillSound;
				audioSource.Play();

				mouseHit.collider.GetComponent<Enemy>().Stun(stunDuration);
				player.GetComponentInParent<Animator>().SetTrigger("TailAttack");
				EndAbility();
			} else {
				Debug.Log("No straight line to the enemy");
			}


		}
	}

	public bool CheckIfPlayerBehindTransf(Transform enemyTranf){
		GameObject tempObj = new GameObject();
		tempObj.transform.SetParent(enemyTranf);
		tempObj.transform.position = enemyTranf.position;
		tempObj.transform.LookAt(player.transform.position);
		float angle = Quaternion.Angle(enemyTranf.rotation, tempObj.transform.rotation);
		Destroy(tempObj);
		if(angle > 115){
			return true;
		}
		return false;
	}

}
