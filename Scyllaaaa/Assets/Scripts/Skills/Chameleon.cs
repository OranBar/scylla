using UnityEngine;
using System.Collections;

public class Chameleon : Skill {

//TODO do this with animation!
	public float chameleonTime = 5f;

	private Vector3 activationPosition;
	private bool chameleonActive = false;

	public override bool ActivationInputDetected (){
		return Input.GetKeyDown(KeyCode.R);
	}

	public override void Activate(){
		if(player.GetComponent<NavMeshAgent>().velocity != Vector3.zero){
			return;
		}

		foreach(Enemy enemy in GameObject.FindObjectOfType<GameManager>().enemies){
			if(enemy.CheckIfInSight(player.transform)){
				DeactivateChameleon();
				return;
			}
		}

		float timeSinceLastActivation = Time.time - timeWhenActivated;
		if(timeSinceLastActivation > coolDownTime){
			timeWhenActivated = Time.time;
			ActivateImpl();
		}

		AudioSource audioSource = GetComponentInParent<AudioSource>();
		audioSource.clip = skillSound;
		audioSource.Play();
		chameleonActive = true;

		for (int i = 0; i < player.GetComponentsInChildren<Renderer> ().Length; i++) {
			Renderer renderer = player.GetComponentsInChildren<Renderer> () [i];
			Color newColor = renderer.material.color;
			newColor.a = newColor.a / 7;
			renderer.material.color = newColor;
		}

		timeWhenActivated = Time.time;
		activationPosition = player.transform.position;



		player.invisible = true;

	}

	protected override void ActivateImpl (){
		base.ActivateImpl();
	}
	/*
	protected override void UseImpl (){
		if(player.GetComponent<NavMeshAgent>().velocity != Vector3.zero){
			return;
		}

		Color newColor = player.GetComponent<Renderer>().material.color;
		newColor.a = newColor.a / 7;
		player.GetComponent<Renderer>().material.color = newColor;

		timeWhenActivated = Time.time;
		activationPosition = player.transform.position;
		player.invisible = true;
	}
*/

	public override void Update(){
		base.Update();
		bool playerHasMoved = (activationPosition != player.transform.position);
		bool skillTimeIsUp = (Time.time - timeWhenActivated) > chameleonTime;
		if(player.invisible && (playerHasMoved || skillTimeIsUp) ){
			EndAbility();
		}
	} 

	public void DeactivateChameleon ()
	{
		player.invisible = false;
		chameleonActive = false;
		for (int i = 0; i < player.GetComponentsInChildren<Renderer> ().Length; i++) {
			Renderer renderer = player.GetComponentsInChildren<Renderer> () [i];
			Color newColor = renderer.material.color;
			newColor.a = newColor.a * 7;
			renderer.material.color = newColor;
		}
	}

	private void StopPlayer(){
		player.GetComponent<NavMeshAgent>().SetDestination(player.transform.position);
		player.GetComponent<NavMeshAgent>().velocity = Vector3.zero;
	}

	public override void EndAbility (){
		base.EndAbility();
		if(chameleonActive){
			DeactivateChameleon();
		}
	}
}
