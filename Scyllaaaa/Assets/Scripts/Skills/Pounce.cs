using UnityEngine;
using System.Collections;

public class Pounce : ActivationSkills
{
	public float pounceSpeed;
	public float pounceAcceleration;
	public float pounceRange;

	public override bool ActivationInputDetected ()
	{
		return Input.GetKeyDown (KeyCode.E);
	}

	protected override float GetRange ()
	{
		return pounceRange;
	}

	protected override void UseSkillImpl (RaycastHit mouseHit){
		bool inRange = (Vector3.Distance (this.transform.position, mouseHit.point) < pounceRange);
		bool hitFloor = (mouseHit.collider.gameObject.layer == LayerMask.NameToLayer("Floor"));
		if(inRange && hitFloor){
			this.timeWhenActivated = Time.time;
			EndAbility();
			StartCoroutine( FastMove(mouseHit.point, pounceSpeed) );
		}
	}

	private IEnumerator FastMoveNavAgent(Vector3 targetPosition, float speed){
		NavMeshAgent navMesh = player.GetComponent<NavMeshAgent>();
		float oldSpeed = navMesh.speed;
		float oldAcceleration = navMesh.acceleration;
		navMesh.speed = pounceSpeed;
		navMesh.acceleration = pounceAcceleration;
		navMesh.SetDestination (targetPosition);
		AudioSource audioSource = GetComponentInParent<AudioSource>();
		audioSource.clip = skillSound;
		audioSource.Play();

		while(Vector3.Distance (player.transform.position, targetPosition) > 2.0f){
			yield return null;
		}

		navMesh.speed = oldSpeed;
		navMesh.acceleration = oldAcceleration;
		player.pouncing = false;
	}

	private IEnumerator FastMove(Vector3 targetPosition, float speed){
		Rigidbody playerRigidb = player.GetComponent<Rigidbody>();
		player.pouncing = true;

		AudioSource audioSource = GetComponentInParent<AudioSource>();
		audioSource.clip = skillSound;
		audioSource.Play();

		while(Vector3.Distance (player.transform.position, targetPosition) > 1.5f){
			if(!player.pouncing){
				EndAbility();
				yield break;
			}
			Vector3 step = Vector3.MoveTowards(player.transform.position, targetPosition, speed * Time.deltaTime);
			playerRigidb.MovePosition(step);
			yield return null;
		}
		player.GetComponent<NavMeshAgent>().SetDestination (targetPosition);
		player.pouncing = false;
//		EndAbility();
	}

	//TODO if hit walls, stun;
}

