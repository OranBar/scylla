using UnityEngine;
using System.Collections;

public abstract class ActivationSkills : Skill
{
	public bool activated;
	public GameObject rangeCircle;
	public bool drawGizmo;

	protected sealed override void ActivateImpl (){
		base.ActivateImpl();
		activated = true;
		ShowRange(true);
	}

	public override void EndAbility(){
		base.EndAbility();

		Vector3 newScale = rangeCircle.transform.localScale;
		newScale.x = newScale.z = 1f;
		rangeCircle.transform.localScale = newScale;

		rangeCircle.SetActive(false);
		activated = false;
	}

	protected virtual void ShowRange(bool enable){
		if(enable){
			rangeCircle.SetActive(true);
			
			//Scale
			Vector3 newScale = rangeCircle.transform.localScale;
			newScale.x = newScale.z = 1f;
			rangeCircle.transform.localScale = newScale;
			
			newScale = rangeCircle.transform.localScale;
			newScale = newScale * GetRange() * 2f;	//TODO: Check if the multiplier is right
			newScale.y = 0.1f;
			rangeCircle.transform.localScale = newScale;
		
		} else {
			rangeCircle.SetActive(false);
		}
	}

	protected abstract float GetRange();

	public void Update(){
		base.Update();
		if(activated && Input.GetMouseButtonDown(0)){
			UseSkill();
		}
	}
	
	private void UseSkill(){
		Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
		
		RaycastHit mouseHit;
		if( Physics.Raycast(mouseRay, out mouseHit, Mathf.Infinity, abilityLayerMask) ){
			UseSkillImpl(mouseHit);
		}
	}

	protected abstract void UseSkillImpl(RaycastHit mouseHit);
	/*
	public void OnDrawGizmos(){
		if(drawGizmo){
			Gizmos.DrawWireSphere(this.transform.position, GetRange());
		}
	}
	*/
}

