using UnityEngine;
using System.Collections;


//TODO not working
public class WallBreaker : Skill {

	public float range = 40f;

	public bool activated = false;

	public override bool ActivationInputDetected () {
		return Input.GetKeyDown (KeyCode.P);
	}

	protected override void ActivateImpl (){
		activated = true;
	}

	public void Update(){
		if(activated && Input.GetMouseButtonDown(0)){
			UseSkill();
		}
	}

	public void UseSkill(){
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		
		RaycastHit mouseHit;
		if( Physics.Raycast(ray, out mouseHit, Mathf.Infinity, abilityLayerMask) ){
			if(mouseHit.transform.CompareTag("Enemy")){
				if( Vector3.Distance(player.transform.position, mouseHit.transform.position) < range ){
					foreach(RaycastHit hit in Physics.RaycastAll(this.transform.position, mouseHit.collider.transform.position, range) ){
						if(hit.collider.gameObject.layer != LayerMask.NameToLayer("Floor")){
							Destroy (hit.collider.gameObject);
						}
					}
					activated = false;
				}
			}	
		}
	}

	public override void EndAbility ()
	{

	}
}
