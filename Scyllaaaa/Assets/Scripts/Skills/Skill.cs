﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public abstract class Skill : MonoBehaviour {

	public AudioClip skillSound;
	public float coolDownTime = 5f;
	public LayerMask abilityLayerMask;
	public Image skillImage;
	public Sprite notSelectedSkill;
	public Sprite selectedSkill;
	public Sprite unavailableSkill;

	private Image previousImage;

	protected PlayerIso player {get;set;}

	protected float timeWhenActivated{get;set;}

	public void Awake(){
		player = GetComponentInParent<PlayerIso>();
		timeWhenActivated = -coolDownTime;
	}

	public virtual void Update(){
		float timeSinceLastActivation = Time.time - timeWhenActivated;
		if(timeSinceLastActivation > coolDownTime){
			skillImage.sprite = notSelectedSkill;
		}
	}

	public abstract bool ActivationInputDetected();

	public virtual void Activate(){
		float timeSinceLastActivation = Time.time - timeWhenActivated;
		if(timeSinceLastActivation > coolDownTime){
			timeWhenActivated = Time.time;
			ActivateImpl();
		}
	}

	protected virtual void ActivateImpl(){
		skillImage.sprite = selectedSkill;
	}

	public virtual void EndAbility(){
		skillImage.sprite = unavailableSkill;
	}
}