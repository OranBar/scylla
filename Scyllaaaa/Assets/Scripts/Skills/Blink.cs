using UnityEngine;
using System.Collections;

public class Blink : ActivationSkills
{
	public float range;

	public override bool ActivationInputDetected ()
	{
		return Input.GetKeyDown(KeyCode.E);
	}

	protected override void UseSkillImpl(RaycastHit mouseHit){
		if(mouseHit.collider.gameObject.layer == LayerMask.NameToLayer ("Floor") ){
			Vector3 pointHit = mouseHit.point;
			
			player.transform.position = pointHit;
			player.GetComponent<NavMeshAgent>().SetDestination(player.transform.position);
			activated = false;
		}
	}

	protected override float GetRange ()
	{
		return range;
	}

}

