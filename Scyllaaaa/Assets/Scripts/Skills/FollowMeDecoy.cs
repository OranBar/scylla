﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class FollowMeDecoy : MonoBehaviour {

	public float enemyInvestigationTime;

	public Dictionary<Enemy, Vector3> enemiesInterruptedWaypoint = new Dictionary<Enemy, Vector3>();

	public void Start(){
		for (int i = 0; i < GetComponentsInChildren<Renderer> ().Length; i++) {
			Renderer renderer = GetComponentsInChildren<Renderer> () [i];
			Color newColor = renderer.material.color;
			newColor.a = newColor.a / 6.5f;
			newColor.g += 0.5f;
			newColor.b += 0.5f;
			renderer.material.color = newColor;
		}
	}


	public void Update(){

		foreach(Enemy enemy in GameObject.FindObjectOfType<GameManager>().enemies){

			bool enemyInSight = enemy.CheckIfInSight(this.transform);

			if(enemyInSight){
				if(enemiesInterruptedWaypoint.ContainsKey(enemy) == false){
					enemiesInterruptedWaypoint.Add(enemy, enemy.GetComponent<NavMeshAgent>().destination);
				}
				if(enemy.GetComponent<PatrolWaypoints>() != null){
					enemy.GetComponent<PatrolWaypoints>().Stop();
				}
				enemy.GetComponent<NavMeshAgent>().SetDestination(this.transform.position);

				if(Vector3.Distance(this.transform.position, enemy.transform.position) < 3f){
					StartCoroutine( ResumePatrol(enemy, enemyInvestigationTime) );
				}
			}
		}
	}

	private IEnumerator ResumePatrol(Enemy enemy, float afterTime ){
		yield return new WaitForSeconds(afterTime);
		Vector3 newMeshTarget;
		if(enemiesInterruptedWaypoint.TryGetValue(enemy, out newMeshTarget)){
			enemy.GetComponent<PatrolWaypoints>().Resume();
			enemy.GetComponent<NavMeshAgent>().SetDestination(newMeshTarget);
		} else {
			Debug.LogError("Why am i here?");
		}
		Destroy(this.gameObject);
	}
}
