﻿using UnityEngine;
using System.Collections;

public class Decoy : ActivationSkills {

	public GameObject decoy;
	public float range;
	public float destroyAfter = 15f;

	public override bool ActivationInputDetected ()
	{
		return Input.GetKeyDown(KeyCode.W);
	}

	protected override void UseSkillImpl (RaycastHit mouseHit) {

		if(Vector3.Distance (player.transform.position, mouseHit.point) > range){
			return;
		}

		this.timeWhenActivated = Time.time;

		AudioSource audioSource = GetComponentInParent<AudioSource>();
		audioSource.clip = skillSound;
		audioSource.Play();

		Vector3 spawnPosition = Vector3.Lerp (player.transform.position, mouseHit.point, 0.10f);
		GameObject decoyInstance = Instantiate(decoy, spawnPosition, player.transform.rotation) as GameObject;
		Destroy(decoyInstance, destroyAfter);
		Physics.IgnoreCollision(player.GetComponent<BoxCollider>(), decoyInstance.GetComponent<BoxCollider>());
		decoyInstance.GetComponent<NavMeshAgent>().SetDestination(mouseHit.point);
		EndAbility();
	}

	protected override float GetRange ()
	{
		return range;
	}
}
