﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(NavMeshAgent))]
public class PatrolWaypoints : MonoBehaviour {
	
	public Transform[] patrolWaypoints;
	public float restartPatrolDelay = 5f;

	public bool drawGizmo;
	private bool stop;

	//TODO StateMachine
	private Vector3 currentTargetMovePosition;
	private NavMeshAgent navMesh;

	private Vector3 lastPosition;

	void Start () {
		navMesh = GetComponent<NavMeshAgent>();
		if(patrolWaypoints.Length == 0){
			return;
		}
		this.transform.position = patrolWaypoints[0].position;
		StartCoroutine( StartPatrol_Coro() );
	}

	protected IEnumerator StartPatrol_Coro(){
		Transform[] reversePatrolWaypoints = patrolWaypoints.Clone() as Transform[];
		Array.Reverse(reversePatrolWaypoints);

		while(true){
			foreach(Transform transf in patrolWaypoints){
				yield return StartCoroutine ( MoveToWaypoint(transf.position) );
			}
			yield return new WaitForSeconds(restartPatrolDelay);
			foreach(Transform transf in reversePatrolWaypoints){
				yield return StartCoroutine ( MoveToWaypoint(transf.position) );
			}
			yield return new WaitForSeconds(restartPatrolDelay);
		}
	}
	
	private IEnumerator MoveToWaypoint(Vector3 targetWaypoint){
		currentTargetMovePosition = targetWaypoint;
		navMesh.SetDestination(targetWaypoint);

		while( Vector3.Distance(this.transform.position, targetWaypoint) > 1.5f || stop ){
			yield return null;
		}
	}

	public void Stop(){
		if(navMesh != null){
			navMesh.destination = this.transform.position;
		}
		stop = true;
	}
	
	public void Resume(){
		if(navMesh != null){
			navMesh.SetDestination(currentTargetMovePosition);
		}
		stop = false;
	}


	void OnDrawGizmos(){
		if(drawGizmo){
			if(patrolWaypoints==null || patrolWaypoints.Length ==0){
				return;
			}
			Transform previous = patrolWaypoints[0];
			for(int i=1; i<patrolWaypoints.Length; i++){
				Gizmos.color = Color.red;
				Gizmos.DrawLine(previous.position, patrolWaypoints[i].position);
				previous = patrolWaypoints[i];
			}
		}
	}


}