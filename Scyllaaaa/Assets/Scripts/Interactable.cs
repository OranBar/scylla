﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public abstract class Interactable : MonoBehaviour {

	public float interactionRange;

	private GameObject player;

	public virtual void Awake(){
		player = GameObject.FindGameObjectWithTag ("Player");
	}

	public abstract void Interact();

	public void OnMouseDown(){
		if(PlayerInInteractionRange()){
			Interact();
		}
	}

	private bool PlayerInInteractionRange(){
		return Vector3.Distance(this.transform.position, player.transform.position) < interactionRange;
	}
}
