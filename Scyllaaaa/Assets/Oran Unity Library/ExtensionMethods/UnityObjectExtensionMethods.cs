using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace OranUnityUtils
{
	public static class UnityObjectExtensionMethods 
	{

		public static IEnumerator ToIEnum(this object unityObj, Action method){
			return ToIEnumImpl(method);
		}

		private static IEnumerator ToIEnumImpl(Action method){
			method();
			yield return null;
		}

		/*
		public static Func<IEnumerator> ToIEnum<T>(this object unityObj, Action<T> method){
			Func<IEnumerator> routine = delegate {
				method(); 
				yield return null; 
			};
			
			return routine;
		}

		private static IEnumerator ToIEnumImpl<T>(Action<T> method){
			method();
			yield return null;
		}
		*/
	}
}

