﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode()]
public class SetHierarchyTag : HiearchyOp {

	public string tagToSet;

	protected override void SingleHook(Transform transf) {
		transf.gameObject.tag = tagToSet;
	}
}
