
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public abstract class RoundSlider : MonoBehaviour {
	
	public RectTransform roundSliderRect;
	public float circeRadiusFromPivot;
	public bool drawGizmo = true;
	public Text centerText;

	protected Slider slider;
	
	public void Awake(){
		slider = GetComponentInParent<Slider>();
	}
	
	public void Update(){
		if(Input.touchCount == 1){
			Touch firstTouch = Input.GetTouch(0);
			
			Vector2 touchLocalToRect;
			if(RectTransformUtility.RectangleContainsScreenPoint(roundSliderRect, firstTouch.position, null)){
				RectTransformUtility.ScreenPointToLocalPointInRectangle(roundSliderRect, firstTouch.position, null, out touchLocalToRect);
				
				Vector2 rectangleCenter = roundSliderRect.rect.center;
				float distanceFromCenter = (touchLocalToRect - rectangleCenter).magnitude;
				if(distanceFromCenter >= circeRadiusFromPivot){
					UpdateSlider(touchLocalToRect);
				}
			}
		}
	}

	protected abstract void UpdateSlider(Vector2 touchLocalToRect);
	
	public void OnDrawGizmos(){
		if(drawGizmo){
			Vector2 rectangleCenter = roundSliderRect.rect.center;
			rectangleCenter.x += circeRadiusFromPivot;
			Gizmos.DrawSphere(rectangleCenter, 5f);
		}
	}


}

