using UnityEngine;
using System.Collections;

[ExecuteInEditMode()]
public abstract class SetHiearchy : MonoBehaviour {
	
	public string ignoreIfNameContains;
	public string setIfNameContains;
	public int recursionDepth = 999;
	public bool execute = false, usaEGetta = true;
	
	
	void Update () {
		if(execute){
			execute = false;
			SetSingle(this.transform);
			SetRecursive(this.transform, 0);
			if(usaEGetta){
				DestroyImmediate(GetComponent<SetHiearchy>());
			}
		}
	}
	
	private void SetRecursive(Transform transf, int depth){
		if(depth > recursionDepth){
			return;
		}
		SetSingle(transf);
		foreach(Transform child in transf){
			SetRecursive(child, depth++);
		}
		
	}
	
	private void SetSingle(Transform transf){
		if(setIfNameContains.Length == 0 || (setIfNameContains.Length > 0 && transf.name.Contains(setIfNameContains) )){
			if(ignoreIfNameContains.Length == 0 || !(ignoreIfNameContains.Length > 0 && transf.name.Contains(ignoreIfNameContains) )){
				SingleHook(transf);
			}
		} 
	}

	protected abstract void SingleHook(Transform transf);
}


