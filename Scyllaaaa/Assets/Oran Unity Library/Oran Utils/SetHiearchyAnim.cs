using UnityEngine;
using System.Collections;

[ExecuteInEditMode()]
public class SetHiearchyAnim : HiearchyOp {

	public AnimationClip anim;
	public bool playAutomatically = false;
	
	protected override void SingleHook(Transform transf) {
		try{
			transf.GetComponent<Animation>().clip = anim;
		}catch (MissingComponentException){
			transf.gameObject.AddComponent<Animation>();
			transf.GetComponent<Animation>().clip = anim;
		}
		transf.gameObject.GetComponent<Animation>().playAutomatically = playAutomatically;
	}

}
