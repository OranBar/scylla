﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RoundAngleSlider : RoundSlider {

	protected override void UpdateSlider(Vector2 touchLocalToRect) {
		Vector2 rectangleCenter = roundSliderRect.rect.center;
		Vector2 touchRelativeToCenterRect = touchLocalToRect - rectangleCenter;
		
		float angle = GetAngleOfVector(touchRelativeToCenterRect);
		if(centerText != null){
			centerText.text = ""+angle;
		}
		slider.value = angle;
	}

	public float GetAngleOfVector(Vector2 touchRelativeToCenterRect){
		float angle = (Mathf.Atan2(touchRelativeToCenterRect.y, -touchRelativeToCenterRect.x));
		angle = angle * 57.2957795f; 	//Convert to degrees
		angle = (angle + 450)%360;	//0 is bottom
		return angle;
	}

}
