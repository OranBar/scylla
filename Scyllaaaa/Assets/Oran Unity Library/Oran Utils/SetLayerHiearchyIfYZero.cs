﻿using UnityEngine;
using System.Collections;

public class SetLayerHiearchy : HiearchyOp {

	protected override void SingleHook (Transform transf)
	{
		if(transf.position.y == 0){
			transf.gameObject.layer = LayerMask.NameToLayer("Floor");
		}
	}

}
