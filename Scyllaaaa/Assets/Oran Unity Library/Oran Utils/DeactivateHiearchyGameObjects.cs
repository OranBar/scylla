﻿using UnityEngine;
using System.Collections;


public class DeactivateHiearchyGameObjects : HiearchyOp {

	protected override void SingleHook (Transform transf)
	{
		transf.gameObject.SetActive (false);
	}
}
