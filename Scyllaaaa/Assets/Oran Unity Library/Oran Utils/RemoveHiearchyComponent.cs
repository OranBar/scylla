﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode()]
public class RemoveHiearchyComponent : HiearchyOp {

	public string componentName;

	protected override void SingleHook(Transform transf) {
		if(transf.GetComponent(componentName) != null){
			DestroyImmediate(transf.GetComponent(componentName));
		}
	}
	

}
